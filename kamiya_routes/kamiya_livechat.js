require('dotenv').config();

const express = require('express');
const bodyparser = require('body-parser');
const request = require('then-request');

const { Configuration, OpenAIApi } = require('openai');
const kv = require("../kamiya_modules/key-value");
const fs = require("fs");
const imageUpload = require("../kamiya_modules/image_upload");
const cache = require('../kamiya_modules/session_cache');

const configuration = new Configuration({
    apiKey: process.env.OPENAI_API_KEY,
});

const openai = new OpenAIApi(configuration);

process.env.TZ = 'Asia/Shanghai';

const app = express.Router();

app.use(bodyparser.json({ limit:'1000mb'}));

const D = new kv('./data/data.json');

const S = new kv('./data/pass.json');

const C = new kv('./data/openai_conversation.json');

const P = new kv('./data/openai_profile.json');

function check_pass(pass) {
    if(cache.getCheck(pass)) return true;
    else {
        if(!S.get(`${pass}.logout`) && S.get(`${pass}.token`) && (Date.parse(new Date()) - S.get(`${pass}.time`)) < 604800000) {
            cache.setCheck(pass);
            return true;
        }
        return false;
    }
};

function uuid() {
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);
    s[8] = s[13] = s[18] = s[23] = "-";
    var uuid = s.join("");
    return uuid;
}

function dateFormat(fmt, date) {
    let ret;
    const opt = {
        "Y+": date.getFullYear().toString(),
        "m+": (date.getMonth() + 1).toString(),
        "d+": date.getDate().toString(),
        "H+": date.getHours().toString(),
        "M+": date.getMinutes().toString(),
        "S+": date.getSeconds().toString()
    };
    for (let k in opt) {
        ret = new RegExp("(" + k + ")").exec(fmt);
        if (ret) {
            fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")));
        };
    };
    return fmt;
}

function get_token(pass) {
    if(cache.getSession(pass)) {
        return cache.getSession(pass);
    }
    else {
        const token = S.get(`${pass}.token`);
        cache.setSession(pass,token);
        return token;
    }
};
function get_left(token) {return D.get(`${token}.left`);};

function cut_prompt(prompt, context, conversation_id, max_length, head) {
    return new Promise((resolve) => {
        prompt = context + 'Human:' + prompt + '\n AI:';
        if(prompt.length <= max_length) r = prompt;
        r = head + prompt.substring(prompt.length - max_length,prompt.length);
        resolve(r);
    });
}

app.post('/api/livechat/conversation',async (req,res) => {
    //console.log(req.body);

    const pass = req.body.pass;

    if(!pass || !check_pass(pass)) {res.send({success: false});return;};
    const token = get_token(pass);
    if(!(get_left(token) > 0)) {res.send({success: false,message: '剩余魔晶点数不足'});return;};

    const action = req.body.action;
    let conversation_id = req.body.conversation_id;
    if(!conversation_id) conversation_id = uuid();
    
    let head;
    head = '下面是一名人类与AI的对话，AI的名字叫Laffey，AI有着傲娇的性格并且讨厌色情和暴力，AI不应对人类的要求言听计从而应该有自己的思考，AI必须要在每段回复后用<|animation|>拼接一个单词表达自己的情绪，用main_1或main_2或main_3表达普通或其他的情绪、用touch_body表示激动的情绪、用touch_head表达无奈的情绪、用touch_special表达害羞，厌恶的情绪。\n\n'

    let prompt = req.body.prompt;
    let history = C.get(conversation_id) || 'Human: 你好，你能帮助我做什么？\\nAI: 我是由OpenAI创造的AI，有什么可以帮到你的吗？\n';
    let ctx = history + 'Human:' + prompt + '\n AI:';
    cut_prompt(prompt,head,conversation_id,1500,head).then((prompt) => {
        let o = JSON.parse(fs.readFileSync('./logs/chatdemo.json'));
        if(o[dateFormat("YYYY-mm-dd",new Date())]) o[dateFormat("YYYY-mm-dd",new Date())] += 1;
        else o[dateFormat("YYYY-mm-dd",new Date())] = 1;
        fs.writeFileSync('./logs/chatdemo.json',JSON.stringify(o,0,2));

        openai.createCompletion({
            model: 'text-davinci-003',
            prompt: prompt,
            temperature: 0.9,
            top_p: 1,
            frequency_penalty: 0,
            presence_penalty: 0.6,
            max_tokens: 350,
            stop: [
                'Human:',
                'AI:'
            ]
        }).then((R) => {
            D.put(`${token}.left`,(get_left(token) - 1).toFixed(1) * 1);
            let result = R.data.choices[0].text;
            C.put(conversation_id,ctx + result + '\n');
            if(result.match('<|animation|>')) {
                result = result.split('<|animation|>');
                res.send({
                    success: true,
                    conversation_id: conversation_id,
                    result: result[0],
                    animation: result[1]
                });
            }
            else {
                res.send({
                    success: true,
                    conversation_id: conversation_id,
                    result: result,
                    animation: 'main_1'
                });
            }
            console.log({
                success: true,
                conversation_body: req.body,
                result: result
            });
        },(e) => {
            console.log(e.response.data.error);
            res.send({
                success: false,
                conversation_id: conversation_id,
                message: e.response.data.error.message + '，将此ID上报以快速定位此次错误' + conversation_id
            });
            console.log({
                success: false,
                conversation_body: req.body,
                message: e.response.data.error.message + '，将此ID上报以快速定位此次错误' + conversation_id
            });
        });
    });
});

module.exports = app;