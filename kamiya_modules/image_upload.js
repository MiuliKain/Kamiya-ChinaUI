require('dotenv').config();

const fs = require('fs');
const request = require('then-request');

const upload_image = (image,token) => {
    return new Promise((resolve, reject) => {
        const nodeL = JSON.parse(fs.readFileSync('./data/backend.json')).storage;
        request('POST',nodeL[Math.floor(Math.random() * nodeL.length)].url + '?token=' + process.env.ADMINPASS,{json: {image: image}}).getBody('utf8').then((R) => {
            R = JSON.parse(R);
            //console.log(R);
            if(R.url == 'Failed to upload this content,contact the admin.') resolve(image);
            else resolve(R.url);
            if(!token) return;
            let uc = JSON.parse(fs.readFileSync('./data/user_content.json'));
            if(!uc[token]) uc[token] = [];
            uc[token].unshift(R);
            fs.writeFileSync('./data/user_content.json',JSON.stringify(uc,0,2));
            //resolve(true);
        },(e) => {
            console.log('WARN 1 user content failed to upload.',e);
            resolve(image);
        });
    });
}

module.exports = upload_image;
